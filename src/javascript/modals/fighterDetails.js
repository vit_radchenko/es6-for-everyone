import { createElement } from '../helpers/domHelper';
import { showModal } from './modal';

export function showFighterDetailsModal(fighter) {
  const title = 'Fighter info';
  const bodyElement = createFighterDetails(fighter);
  showModal({ title, bodyElement });
}

function createFighterDetails(fighter) {
  const { name, attack, defense, health, source } = fighter;

  const fighterDetails = createElement({ tagName: 'div', className: 'modal-body' });
  const detaisInfo = createElement({ tagName: 'div', className: 'fighter-details' });
  const nameElement = createElement({ tagName: 'span', className: 'fighter-name' });
  const attackElement = createTag('span', attack, 'Attack', 'fighter-attack');
  const defenseElement = createTag('span', defense, 'Defense', 'fighter-defense');
  const healthElement = createTag('span', health, 'Health', 'fighter-health');

  nameElement.innerText = name;
  detaisInfo.append(nameElement, attackElement, defenseElement, healthElement);
  fighterDetails.append(detaisInfo, createImage(source));

  return fighterDetails;
}

function createTag(tagName, param, dispalyText, className) {
  const element = createElement({ tagName, className });
  element.innerText = `${dispalyText}: ` + param;

  return element;
}

function createImage(source) {
  const attributes = { src: source };
  const imgElement = createElement({ tagName: 'img', className: 'fighter-image', attributes });

  return imgElement;
}