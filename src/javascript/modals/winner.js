import { showModal } from './modal';
import { createElement } from '../helpers/domHelper';

export function showWinnerModal(fighter) {
  const title = 'Winner';
  const bodyElement = createWinnerDetails(fighter);
  showModal({ title, bodyElement });
}

function createWinnerDetails(fighter) {
  const { name, source } = fighter;

  const fighterDetails = createElement({ tagName: 'div', className: 'modal-body' });
  const nameElement = createElement({ tagName: 'span', className: 'fighter-name' });

  nameElement.innerText = name;
  fighterDetails.append(nameElement, createElement({ tagName: 'br' }), createImage(source));

  return fighterDetails;
}

function createImage(source) {
  const attributes = { src: source };
  const imgElement = createElement({ tagName: 'img', className: 'fighter-image', attributes });

  return imgElement;
}