const MIN_DAMAGE = 0;
const MAX_ROUND = 1000;

export function fight(firstFighter, secondFighter) {
  const cloneFirstFighter = Object.assign({}, firstFighter);
  const cloneSecondFighter = Object.assign({}, secondFighter);

  for (let i = 0; i < MAX_ROUND; i++) {
    let damageFromFirstFighter = getDamage(cloneFirstFighter, cloneSecondFighter);
    cloneSecondFighter.health = cloneSecondFighter.health - damageFromFirstFighter;

    if (cloneSecondFighter.health <= 0) {
      return cloneFirstFighter;
    }

    let damageFromSecondFighter = getDamage(cloneSecondFighter, cloneFirstFighter);
    cloneFirstFighter.health = cloneFirstFighter.health - damageFromSecondFighter;
    
    if (cloneFirstFighter.health <= 0) {
      return cloneSecondFighter;
    }
  }

  return cloneFirstFighter.health > cloneSecondFighter.health ? cloneFirstFighter : cloneSecondFighter;
}

export function getDamage(attacker, enemy) {
  let damage = getHitPower(attacker) - getBlockPower(enemy);
  return Math.max(damage, MIN_DAMAGE);
}

export function getHitPower(fighter) {
  const { attack } = fighter;
  const criticalHitChance = getRandomChance();

  return attack * criticalHitChance;
}

export function getBlockPower(fighter) {
  const { defense } = fighter;
  const dodgeChance = getRandomChance();

  return defense * dodgeChance;
}

function getRandomChance() {
  return Math.random() + 1;
}
